<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayDescription extends Model
{
    protected $table = 'play_description';
}
