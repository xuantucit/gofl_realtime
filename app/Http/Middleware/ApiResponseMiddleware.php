<?php

namespace App\Http\Middleware;

use Closure;

class ApiResponseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $response->withHeaders([
            'Access-Control-Allow-Origin' => '*',
            'Content-type'                => 'application/json;charset=utf-8',
            'Content-Encoding'            => 'chunked',
            'Cache-Control'               => 'max-age=31536000, public',
            'Keep-Alive'                  => 'timeout=5, max=100',
            //            'Content-Encoding'  => 'gzip',
            //            'Transfer-Encoding' => 'gzip',
        ]);

        return $response;
    }
}
