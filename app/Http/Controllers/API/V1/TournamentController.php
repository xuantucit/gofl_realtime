<?php

namespace App\Http\Controllers\API\V1;

use App\Models\Customer;
use App\Models\Group;
use App\Models\GroupCustomer;
use App\Models\Match;
use App\Models\Play;
use App\Models\Team;
use App\Models\TeamCustomer;
use App\Models\TeamMatch;
use App\Models\Tournament;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TournamentController extends Controller
{
    public function getList()
    {
        try
        {
            $tournaments = Tournament::select('tournament_id','slug','name','start_date','end_date','status','supplier_id','type','background')->where('deleted', 0)->orderBy('tournament_id','DESC')->get();

            return $this->dataSuccess('Lấy danh sách giải đấu thành công',$tournaments,200);
        }
        catch (\Exception $exception)
        {
          return  $this->dataError($exception->getMessage(),[],422);
        }
    }

    public function getListFlight(Request $request)
    {
        try
        {
            $tournament = Tournament::where('tournament_id',$request->tournament_id)->first();

          if($tournament->type == 1 || $tournament->type == 2)
          {
              $tournament['flights'] = Group::select('name','group_id as flight_id')->where('tournament_id', $request->tournament_id)->where('deleted',0)->get();
          }
          else
          {
              $tournament['flights'] = TeamMatch::select('name','match_id as flight_id')->where('tournament_id', $request->tournament_id)->where('deleted',0)->get();
          }

            return $this->dataSuccess('Lấy danh sách giải đấu thành công',$tournament,200);
        }
        catch (\Exception $exception)
        {
            return  $this->dataError($exception->getMessage(),[],422);
        }
    }

    public function listGolfer(Request $request)
    {
        try
        {
            $name = $request->name ? $request->name : '';
            $branch = $request->branch ? $request->branch : '';
            $golfers = GroupCustomer::where('tournament_id',$request->tournament_id)->with(array('customer'=>function($query) use($name,$branch){
               $query->select('customer_id','name','code','status')->where('name','LIKE',"%$name%");
           }))->where('bang','LIKE',"%$branch%")->get();


            foreach ($golfers as $golfer)
            {
                $golfer['play'] = Play::where('play_id',$golfer->play_id)->first();
                if($golfer['customer'] != null)
                {
                    $list[] = $golfer;
                }

            }

            $tournament = Tournament::where('tournament_id',$request->tournament_id)->first();
            $pars = unserialize($tournament->par);
            foreach ($pars as $key => $row) {
                $data['pars']['hole_' . ($key + 1)] = $row;
            }


            $golfer_2['players'] = $list;
            $golfer_2['par']= $data['pars'];

            if(count($golfer_2) == 0)
            {
                return $this->dataError('Không tìm thấy Golffer nào? Vui lòng thử lại tên khác.',[],422);
            }

          return $this->dataSuccess('Lấy danh sách golfers thành công',$golfer_2,200);

        }
        catch (\Exception $exception)
        {
            return $this->dataError('Lấy danh sách golfer thất bại',[],422);
        }
    }

    public function listGolfer2(Request $request)
    {
        try
        {
            $tournament = Tournament::where('tournament_id',$request->tournament_id)->first(); // Giải đấu // 18

            $round = $tournament['round'];  //

            if($tournament->type == 1 || $tournament->type == 2)
            {
                $list = [];

                for($i = 1; $i <= $round; $i++)
                {
                    $golfers = GroupCustomer::where('tournament_id',$request->tournament_id)->where('group_id',$request->flight_id)->with(array('customer'=>function($query){
                        $query->select('customer_id','name','code','status');
                    }))->get();

                    foreach ($golfers as $golfer)
                    {
                        $golfer['play'] = Play::where('play_id',$golfer->play_id)->first();
                        if($golfer['customer'] != null)
                        {
                            $list[] = $golfer;
                        }

                    }

                    $tournament = Tournament::where('tournament_id',$request->tournament_id)->first();
                    $pars = unserialize($tournament->par);
                    foreach ($pars as $key => $row) {
                        $data['pars']['hole_' . ($key + 1)] = $row;
                    }

                    $dt['round_name'] = "Round $i";
                    $dt['players'] =  $list;
                    $dt['par']= $data['pars'];


                    $datas[] = $dt;

                    $list = [];
                }
            }
            else
            {
                return
                $list_groups = Group::where('tournament_id',$request->tournament_id)->get();

                $group_id_1 = $list_groups[0];
                $group_id_2 = $list_groups[1];

                $list_team_golfers_1 = Play::whereHas('group_customers', function ($e) use ($request,$group_id_1){
                    $e->where('tournament_id',$request->tournament_id)->where('match_id',$request->flight_id)->where('group_id',$group_id_1['group_id']);
                })->get();

                $list_team_golfers_2 = Play::whereHas('group_customers', function ($e) use ($request,$group_id_2){
                    $e->where('tournament_id',$request->tournament_id)->where('match_id',$request->flight_id)->where('group_id',$group_id_2['group_id']);
                })->get();

              foreach($list_team_golfers_1 as $key => $item) {
                  $list_team_golfers_1[$key]['customer']= Customer::select('customer_id', 'code', 'name', 'gender', 'status')->where('customer_id', $item->group_customer->customer_id)->first();
                  $list_team_golfers_1[$key]['team_name'] = $list_groups[0]['name'];
              }
                foreach($list_team_golfers_2 as $key => $item)
                {
                    $list_team_golfers_2[$key]['customer'] = Customer::select('customer_id','code','name','gender','status')->where('customer_id',$item->group_customer->customer_id)->first();
                    $list_team_golfers_1[$key]['team_name'] = $list_groups[0]['name'] ;
                }


                $time_1 = TeamMatch::where('type',1)->where('match_id',$request->flight_id)->where('tournament_id',$request->tournament_id)->orderBy('match_id','DESC')->first();
                if( $time_1)
                {
                    $data[0][0]['time'] =  $time_1->happen_time;
                    $data[0][0]['play_type'] = 'foursome' ;
                    $data[0][0]['teams'][0] = $list_team_golfers_1 ;
                    $data[0]['teams'][0]['team_name'] = $list_groups[0]['name'] ;
                    $data[0][0]['teams'][1] = $list_team_golfers_2 ;
                    $data[0]['teams'][1]['team_name'] = $list_groups[1]['name'] ;

                }

                $time_2 = TeamMatch::where('type',2)->where('match_id',$request->flight_id)->where('tournament_id',$request->tournament_id)->orderBy('match_id','DESC')->first();
                if($time_2)
                {
                    $data[1][0]['time'] =  $time_2->happen_time;
                    $data[1][0]['play_type'] = 'fourball' ;
                    $data[1][0]['teams'][0] = $list_team_golfers_1 ;
                    $data[1]['teams'][0]['team_name'] = $list_groups[0]['name'] ;
                    $data[1][0]['teams'][1] = $list_team_golfers_2 ;
                    $data[1]['teams'][1]['team_name'] = $list_groups[1]['name'] ;

                }

                $time_3 = TeamMatch::where('type',3)->where('match_id',$request->flight_id)->where('tournament_id',$request->tournament_id)->orderBy('match_id','DESC')->first();
                if($time_3)
                {
                    $data[2][0]['time'] =  $time_3->happen_time;
                    $data[2][0]['play_type'] = 'single' ;
                    $data[2][0]['teams'][0] = $list_team_golfers_1 ;
                    $data[2]['teams'][0]['team_name'] = $list_groups[0]['name'] ;
                    $data[2][0]['teams'][1] = $list_team_golfers_2 ;
                    $data[2]['teams'][1]['team_name'] = $list_groups[1]['name'] ;

                }


                if(!empty($data[0]))
                {

                    $dt['time'] = $data[0][0]['time'];
                    $dt['play_type'] = $data[0][0]['play_type'];
                    $dt['teams'][0]['team_name'] = $list_groups[0]['name'];
                    $dt['teams'][0]['players'] = $data[0][0]['teams'][0];
                    $dt['teams'][1]['team_name'] = $list_groups[1]['name'];
                    $dt['teams'][1]['players'] = $data[0][0]['teams'][1];
                    $datas[] = $dt;
                }


                if(!empty($data[1]))
                {

                    $dt['time'] = $data[1][0]['time'];
                    $dt['play_type'] = $data[1][0]['play_type'];
                    $dt['teams'][0]['team_name'] = $list_groups[0]['name'];
                    $dt['teams'][0]['players'] = $data[1][0]['teams'][0];
                    $dt['teams'][1]['team_name'] = $list_groups[1]['name'];
                    $dt['teams'][1]['players'] = $data[1][0]['teams'][1];

                    $datas[] = $dt;

                }


                if(!empty($data[2]))
                {
                    $dt['time'] = $data[2][0]['time'];
                    $dt['play_type'] = $data[2][0]['play_type'];
                    $dt['teams'][0]['team_name'] = $list_groups[0]['name'];
                    $dt['teams'][0]['players'] = $data[2][0]['teams'][0];
                    $dt['teams'][1]['team_name'] = $list_groups[1]['name'];
                    $dt['teams'][1]['players'] = $data[2][0]['teams'][1];

                    $datas[] = $dt;
                }


            }

            return $this->dataSuccess('Lấy danh sách nhập điểm thành công',$datas,200);
        }catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),[],422);
        }
    }
}
