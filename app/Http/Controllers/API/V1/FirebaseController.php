<?php

namespace App\Http\Controllers\API\V1;

use App\Models\GroupCustomer;
use App\Models\Play;
use App\Models\Tournament;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use SimpleXMLElement;
use Spatie\ArrayToXml\ArrayToXml;

class FirebaseController extends Controller
{
    public function pushTournament(Request $request)
    {
        try{

            $tournament_id = $request->tournament_id;
            $tournament = Tournament::where('tournament_id',$tournament_id)->first();
            $list = GroupCustomer::where('tournament_id',$tournament_id)->get();
            $data['list'] = $list;


            switch ($tournament->type){
                case 1:

                    $data['tournament_type'] = 'Giải chuyên nghiệp';
                    $data['type'] = $tournament->type;
                    $data['total_round'] = $tournament->round;
                    $data['total_round'] = $tournament->name;

                case 2:

                    $data['tournament_type'] = 'Giải nghiệp dư';
                    $data['type'] = $tournament->type;
                    $data['total_round'] = $tournament->round;
                    $data['name'] = $tournament->name;
             }


            $serviceAccount = ServiceAccount::fromJsonFile('../public/firebase/firebase.json');
            $firebase       = (new Factory())
                ->withServiceAccount($serviceAccount)
                ->create();
            $db             = $firebase->getDatabase();

            $db->getReference('tournament/' . $tournament_id)->set($data);



            return $this->dataSuccess('Lấy danh sách giải đấu thành công',$data,200);

        }catch (\Exception $exception)
        {
            return $this->dataError('Cập nhật dữ liệu Firebase thất bại',[],422);
        }
    }

    public function getTournamentFromFirebase(Request $request)
    {
       try{
           $serviceAccount = ServiceAccount::fromJsonFile('../public/firebase/firebase.json');
           $firebase       = (new Factory())
               ->withServiceAccount($serviceAccount)
               ->create();
           $db             = $firebase->getDatabase();

           $reference = $db->getReference('/tournament/'.$request->id);


           $snapshot = $reference->getSnapshot();
           $value = $snapshot->getValue();

           $result = ArrayToXml::convert($value);
           print_r($result);die();

           return $this->dataSuccess('Lấy danh sách giải đấu thành công.',$value,200);
       }catch (\Exception $exception){
           return $this->dataError('Lấy danh sách giải đấu thất bại',[],422);
       }
    }


    public function pushTournamentDetail(Request $request)
    {
        try{


            $tournament_id = $request->tournament_id;
            $tournament = Tournament::where('tournament_id',$request->tournament_id)->first();
            $data['name'] = $tournament->name;
            $round = $tournament->round;
            $data['total_round']= $tournament->round;
            $data['type'] = $tournament->type;

            $pars = $tournament->par;
            $pars = unserialize($pars);

            foreach ($pars as $key => $row):
                $tour_array['pars']['hole_' . ($key + 1)] = $row;
            endforeach;
            $data['pars'] = $tour_array['pars'];
            $total_pars = 0;
            $total_out = 0;
            $total_in = 0;
            for ($i = 1; $i <= 18; $i++) {
                $val = isset($tour_array['pars'][$i - 1]) ? $tour_array['pars'][$i - 1] : 0;
                if($i <= 9):
                    $total_out += (int)$val;
                else:
                    $total_in += (int)$val;
                endif;
                $total_pars += $val;
            }
            $data['total_pars'] = $total_pars;
            $data['out_pars'] = $total_out;
            $data['in_pars'] = $total_in;


           for($i = 1; $i <= $round; $i++)
           {
               switch ($tournament->type){
                   case 1:
                   case 2:
                       $data['tournament_type'] = 'Giải chuyên nghiệp';

                        $golfer = GroupCustomer::with(array('play'=>function($query) use ($request){
                           $query->where('round',$request->round);
                       }))->where('tournament_id',$request->tournament_id)->get();


                       foreach ($golfer as $item)
                       {
                           $data['list']['golfers']["$item->customer_id"] = $item['play'];
                       }

                       foreach ($data['list']['golfers'] as $key => $item){
                           $total_play_in = 0;
                           $total_play_out = 0;
                           for($j = 1; $j < 19; $j++):
                               $hole = ($item['hole_' . $j] != 0) ? $item['play']['hole_' . $j] : 0;
                               if($j < 10) $total_play_out += $hole;
                               else $total_play_in += $hole;
                               $array_point['hole_'. $j] = $hole;
                           endfor;
                           $item['out'] = $total_play_out;
                           $item['in'] = $total_play_in;
                           $item['total'] = $total_play_in + $total_play_out;
                           $item['net'] = $item['gross'] - $item['hdc'];

                       }
               }

               $serviceAccount = ServiceAccount::fromJsonFile('../public/firebase/firebase.json');
               $firebase       = (new Factory())
                   ->withServiceAccount($serviceAccount)
                   ->create();
               $db             = $firebase->getDatabase();

               $db->getReference('tournament-detail/' . $tournament_id.'/'.$i)->set($data);
           }

            return $this->dataSuccess('Lấy danh sách giải đấu thành công',$data,200);

        }catch (\Exception $exception){
            return $this->dataError($exception->getMessage(),[],422);
        }

    }


    public function getTournamentDetailFromFirebase(Request $request){
        try{

            $serviceAccount = ServiceAccount::fromJsonFile('../public/firebase/firebase.json');
            $firebase       = (new Factory())
                ->withServiceAccount($serviceAccount)
                ->create();
            $db             = $firebase->getDatabase();

            $reference = $db->getReference('/tournament-detail/'.$request->tournament_id.'/'.$request->round_id);


            $snapshot = $reference->getSnapshot();
            $value = $snapshot->getValue();




            $value['list']['golfers'] = array_values($value['list']['golfers']);

            $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><data></data>');
            $this->array_to_xml($value,$xml);
            header("Content-type: text/xml");

            print $xml->asXML();die;

            $result = ArrayToXml::convert($value);
//            print_r($result);die();
            $this->array_to_xml($value, $result);
            header("Content-type: text/xml");
            print $result->asXML();die;
            return $this->dataSuccess('Lấy danh sách giải đấu thành công.',$value,200);
        }catch (\Exception $exception){
            return $this->dataError($exception->getMessage(),[],422);
        }
    }

   public function array_to_xml( $data, $xml_data ) {
        foreach( $data as $key => $value ) {

            if( is_numeric($key) ){
                $key = 'golfers';
            }
            if( is_array($value) ) {
                $subnode = $xml_data->addChild($key);
                $this->array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

    public function pushRankFirebase(Request $request)
    {
        $tournament = Tournament::where('tournament_id',$request->tournament_id)->first();
        $data['name'] = $tournament->name;
        $data['start_date'] = $tournament->start_date;
        $data['end_date'] = $tournament->end_date;

        $golfers = GroupCustomer::with(array('play'=>function($query) use ($request){
            $query->where('round',$request->round);
        }))->where('tournament_id',$request->tournament_id)->get();

        foreach ($golfers as $golfer)
        {

            $golfer['play']->makeHidden(['hole_1','hole_2','hole_3','hole_4','hole_5','hole_6','hole_7','hole_8','hole_9','hole_10','hole_11','hole_12','hole_13','hole_14','hole_15','hole_16','hole_17','hole_18']);
            $ranks[] = $golfer['play'];
        }


        for($i = 0; $i < count($ranks) - 1; $i++)
        {
            for($j = 1; $j < count($ranks); $j++)
            {
                if($ranks[$i]['point'] < $ranks[$j]['point']){
                    $th = $ranks[$i];
                    $ranks[$i] = $ranks[$j];
                    $ranks[$j] = $th;
                }
            }
        }

        for ($i = 0; $i <count($ranks);$i++)
        {
            $ranks[$i]['rank'] = $i+1;
        }

        $data['ranks'] = $ranks;

        $serviceAccount = ServiceAccount::fromJsonFile('../public/firebase/firebase.json');
        $firebase       = (new Factory())
            ->withServiceAccount($serviceAccount)
            ->create();
        $db             = $firebase->getDatabase();

        $db->getReference('ranks/' . $request->tournament_id.'/'.$request->round)->set($data);

        return $this->dataSuccess('Cập nhật bảng xếp hạng thành công',$data,200);
    }

    public function getRankFirebase(Request $request)
    {
        $serviceAccount = ServiceAccount::fromJsonFile('../public/firebase/firebase.json');
        $firebase       = (new Factory())
            ->withServiceAccount($serviceAccount)
            ->create();
        $db             = $firebase->getDatabase();

        $reference = $db->getReference('/ranks/'.$request->tournament_id.'/'.$request->round_id);


        $snapshot = $reference->getSnapshot();
        $value = $snapshot->getValue();

        return $value[$request->round];
    }
}
