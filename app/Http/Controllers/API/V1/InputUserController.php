<?php

namespace App\Http\Controllers\API\V1;

use App\Models\GroupCustomer;
use App\Models\Play;
use App\PlayDescription;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class InputUserController extends Controller
{
    public function inputPointUser(Request $request)
    {
        try{
                $play = Play::where('play_id',$request->play_id)->first();
                $hole_id = $request->hole_id;

                $play_description = PlayDescription::where('play_id',$request->play_id)->where('hole',$request->hole_id)->first();



            if($play_description)
                {

                    $play_description->pos = $request->pos;
                    $play_description->save();

                }
                else
                {
                    $play_description =  new PlayDescription();
                    $play_description->play_id = $play->id;
                    $play_description->round = $play->round;
                    $play_description->hole = $request->hole_id;
                    $play_description->pos = $request->pos;
                    $play_description->save();
                }

//                $user_hole = User::where('id',Auth::user()->id)->first();

//                if($user_hole->hole_id != $hole_id)
//                {
//                    return $this->dataError('Bạn không được phép nhập điểm cho hole này',[],200);
//                }

            $group_customer  = GroupCustomer::where('play_id',$play->play_id)->first();
                switch ($hole_id)
                {
                    case 1 :
                        $play->hole_1 = $request->point;
                        break;
                    case 2 :
                        $play->hole_2 = $request->point;
                        break;
                    case 3 :
                        $play->hole_3 = $request->point;
                        break;
                    case 4 :
                        $play->hole_4 = $request->point;
                        break;
                    case 5 :
                        $play->hole_5 = $request->point;
                        break;
                    case 6 :
                        $play->hole_6 = $request->point;
                        break;
                    case 7 :
                        $play->hole_7 = $request->point;
                        break;
                    case 8 :
                        $play->hole_8 = $request->point;
                        break;
                    case 9 :
                        $play->hole_9 = $request->point;
                        break;
                    case 10 :
                        $play->hole_10 = $request->point;
                        break;
                    case 11 :
                        $play->hole_11 = $request->point;
                        break;
                    case 12 :
                        $play->hole_12 = $request->point;
                        break;
                    case 13 :
                        $play->hole_13 = $request->point;
                        break;
                    case 14 :
                        $play->hole_14 = $request->point;
                        break;
                    case 15 :
                        $play->hole_15 = $request->point;
                        break;
                    case 16 :
                        $play->hole_16 = $request->point;
                        break;
                    case 17 :
                        $play->hole_17 = $request->point;
                        break;
                    case 18 :
                        $play->hole_18 = $request->point;
                        break;
                }

            // Tính point

            $point = $play->hole_1 + $play->hole_2 + $play->hole_3 + $play->hole_4 + $play->hole_5 + $play->hole_6 + $play->hole_7 + $play->hole_8 + $play->hole_9 + $play->hole_10 + $play->hole_11 + $play->hole_12 + $play->hole_13 + $play->hole_14 + $play->hole_15 + $play->hole_16 + $play->hole_17 + $play->hole_18;
                $play->point = $point;
                $play->save();

            $total_play_in = 0;
            $total_play_out = 0;
            for($j = 1; $j < 19; $j++):
                $hole = ($play['hole_' . $j] != 0) ? $play['hole_' . $j] : 0;
                if($j < 10) $total_play_out += $hole;
                else $total_play_in += $hole;
            endfor;

            $total = $total_play_in + $total_play_out;


            $serviceAccount = ServiceAccount::fromJsonFile('../public/firebase/firebase.json');
            $firebase       = (new Factory())
                ->withServiceAccount($serviceAccount)
                ->create();
            $db             = $firebase->getDatabase();
//
////            file_get_contents('http://golf-api.fvet.vn/api/v1/firebase/push-tournament-detail?tournament_id=24&round=1');
//            file_get_contents('http://golf-api.fvet.vn/api/v1/firebase/push-rank-firebase?tournament_id=24&round=1');
////
            $db->getReference('tournament-detail/' . $request->tournament_id.'/'.$request->round.'/list/golfers/'.$group_customer->customer_id.'/total')->set($total);
            $db->getReference('tournament-detail/' . $request->tournament_id.'/'.$request->round.'/list/golfers/hole_'.$request->hole_id)->set($total);
            $db->getReference('tournament-detail/' . $request->tournament_id.'/'.$request->round.'/list/golfers/'.$group_customer->customer_id.'/in')->set($total_play_in);
            $db->getReference('tournament-detail/' . $request->tournament_id.'/'.$request->round.'/list/golfers/'.$group_customer->customer_id.'/out')->set($total_play_out);


                return $this->dataSuccess('Cập nhật điểm thành công',$play,200);

        }
        catch (\Exception $exception)
        {
            return $this->dataSuccess($exception->getMessage(),[],422);
        }
    }
}
