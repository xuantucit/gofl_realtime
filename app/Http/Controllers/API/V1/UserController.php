<?php

namespace App\Http\Controllers\API\V1;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function register(Request $request)
    {
        try
        {

            $validator = \Validator::make($request->all(), [

                'email'    => 'required|unique:users',
                'password' => 'required'
            ], [
                'email.required'    => 'Email bắt buộc',
                'password.required' => 'Password bắt buộc nhập',
                'email.unique' => 'Email đã tồn tại',
            ]);

            if($validator->fails()) {
                return $this->dataError('lỗi xác thực', $validator->errors(), 422);
            }


            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = bcrypt($request->password);
            $user->hole_id = $request->hole_id;
            $user->save();

            $token = $user->createToken('REGISTER')->accessToken;
            $user['token'] = $token;

            return $this->dataSuccess('Đăng kí thành công',$user,200);
        }
        catch (\Exception $exception)
        {
           return $this->dataError($exception->getMessage(),'Đăng kí thất bại',422);
        }
    }

    public function login(Request $request)
    {
        try
        {
            $validator = \Validator::make($request->all(), [

                'email'    => 'required',
                'password' => 'required'
            ], [
                'email.required'    => 'Email bắt buộc',
                'password.required' => 'Password bắt buộc nhập'
            ]);

            if($validator->fails()) {
                return $this->dataError('lỗi xác thực', $validator->errors(), 422);
            }



            if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = User::where('email',$request->email)->first();
                Auth::login($user);
                $user = Auth::user();
                $token = $user->createToken('LOGIN')->accessToken;
                $user['token'] = $token;

                return $this->dataSuccess('Đăng nhập thành công',$user,200);
            }
            else
            {
                return $this->dataError('Tên đăng nhập hoặc mật khẩu không đúng',[],422);
            }
        }catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),[],422);
        }

    }

    public function logout(){
        try{

            $accessToken = Auth::user()->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                    'revoked' => true
                ]);
            $accessToken->revoke();

            return $this->dataSuccess('Đăng xuất thành công',[],200);

        }catch (\Exception $exception){
            $this->dataError('Đăng xuất thất bại',[],422);
        }

    }

}
