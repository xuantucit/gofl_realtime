<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    protected $table = 'kk_tournament';

    protected $appends = ['golf_course','tournament_type'];

    public function getGolfCourseAttribute()
    {
        $name =  Supplier::where('supplier_id',$this->supplier_id)->first();

        if($name)
        {
            return $name->name;
        }
        else
        {
            return null;
        }
    }

    public function getTournamentTypeAttribute()
    {
        $type = Type::where('id',$this->type)->first();
        if($type)
        {
            return $type->name;
        }
        else{
            return null;
        }

    }

    public function getBackgroundAttribute($val)
    {
        return $val = 'http://giaigolf.com/upload/'.$val;
    }
}
