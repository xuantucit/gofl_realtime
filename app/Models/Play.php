<?php

namespace App\Models;

use App\PlayDescription;
use Illuminate\Database\Eloquent\Model;
use Mpociot\Firebase\SyncsWithFirebase;

class Play extends Model
{
    use SyncsWithFirebase;
    protected $table = 'play';
    protected $appends = ['pos','thru','gross','name','avatar','group'];
    protected $hidden = ['id','play_id','round','total_sticks'];



    public function group_customers()
    {
        return $this->hasMany(GroupCustomer::class,'play_id','play_id');
    }

    public function group_customer()
    {
        return $this->hasOne(GroupCustomer::class,'play_id','play_id');
    }

    public function getPosAttribute()
    {
        $pos =  PlayDescription::where('play_id',$this->play_id)->first();
        if($pos)
        {
            return $pos->pos;
        }
        else{
            return 0;
        }

    }

    public function getHdcAttribute($val)
    {
        if($val == null)
        {
            return $val = 0;
        }
        else
        {
            return $val;
        }
    }

    public function getThruAttribute()
    {
        return $this->current_hole;
    }

    public function getGrossAttribute()
    {
        return $this->total_sticks;
    }

    public function getNameAttribute()
    {
        $group_customer = GroupCustomer::where('play_id',$this->play_id)->first();
        if($group_customer){
            $cus = Customer::where('customer_id',$group_customer->customer_id)->first();
            if($cus)
                return $cus->name;
            else
                return null;
        }else
            return null;


    }

    public function getAvatarAttribute()
    {
        $group_customer = GroupCustomer::where('play_id',$this->play_id)->first();

        if($group_customer){
            $cus = Customer::where('customer_id',$group_customer->customer_id)->first();
            if($cus)
                return $cus->avatar;
            else
                return null;
        }else
            return null;

    }

    public function getGroupAttribute()
    {
        $group_customer = GroupCustomer::where('play_id',$this->play_id)->first();

        if($group_customer){
            $group = Group::where('group_id',$group_customer->group_id)->first();
            if($group)
                return $group->name;
            else
                return null;
        }else
            return null;
    }
}
