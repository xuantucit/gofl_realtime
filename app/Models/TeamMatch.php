<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamMatch extends Model
{
    protected $table = 'team_match';

    public function group_customers()
    {
        return $this->hasMany(GroupCustomer::class,'play_id','play_id');
    }

}
