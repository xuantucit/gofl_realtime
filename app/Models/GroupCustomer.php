<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupCustomer extends Model
{
    protected $table = 'group_customer';
    protected $appends = ['group','gross','total_sticks','point'];


    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id','customer_id');
    }


    public function play()
    {
        return $this->belongsTo(Play::class,'play_id','play_id');
    }

    public function getGroupAttribute()
    {
        return Group::where('group_id',$this->group_id)->first()->name;
    }

    public function getPointAttribute()
    {
        $point =  Play::where('play_id',$this->play_id)->first();
        {
            if($point)
            {
                return $point->point;
            }
            else
            {
                return 0;
            }
        }
    }

    public function getGrossAttribute()
    {
        $play =  Play::where('play_id',$this->play_id)->first();
        if($play)
        {
            return $play->current_hole;
        }
        else
            return 0;
    }
    public function getTotalSticksAttribute()
    {
        $total =  Play::where('play_id',$this->play_id)->first();
        if($total)
        {
            return $total->total_sticks;
        }
        else
        {
            return 0;
        }
    }
}
