<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamCustomer extends Model
{
    protected $table = 'team_customer';
}
