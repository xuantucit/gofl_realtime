<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API\V1'], function() {
    Route::group(['prefix' => 'user'],function(){
        Route::post('/register','UserController@register');
        Route::post('/login','UserController@login');
    });

    Route::group(['prefix' => 'tournament','middleware' => 'auth:api'],function(){
        Route::get('/list','TournamentController@getList');
        Route::get('/list-flight','TournamentController@getListFlight');
        Route::get('/list-golfer','TournamentController@listGolfer');
        Route::get('/list-golfer-2','TournamentController@listGolfer2');


    });

    Route::group(['middleware' => 'auth:api'],function(){

        Route::post('/logout','UserController@logout');


    });

    Route::group(['prefix' => 'user','middleware' => 'auth:api'],function(){
        Route::post('/input-point-user','InputUserController@inputPointUser');
    });

    Route::group(['prefix' => 'firebase','middleware' => 'auth:api'],function(){
        Route::post('/push-tournament','FirebaseController@pushTournament');
    });


    Route::group(['prefix' => 'firebase'],function(){
        Route::get('/get-tournament/{id}','FirebaseController@getTournamentFromFirebase');
        Route::get('/get-tournament-detail','FirebaseController@getTournamentDetailFromFirebase');
        Route::get('/push-tournament-detail','FirebaseController@pushTournamentDetail');
        Route::get('/push-rank-firebase','FirebaseController@pushRankFirebase');
        Route::get('/get-rank-firebase','FirebaseController@getRankFirebase');


    });



});
