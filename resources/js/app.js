require('./bootstrap');
import Vue from 'vue'
import VueRouter from 'vue-router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(VueRouter)
Vue.use(ElementUI)
import App from './components/layouts/App'
import Live from './components/pages/Live'
import Home from './components/pages/Home'
import Score from './components/pages/Score'
import Realtime from './components/pages/Realtime'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/live',
            name: 'live',
            component: Live,
        },
        {
            path: '/score',
            name: 'score',
            component: Score,
        },
        {
            path: '/realtime',
            name: 'realtime',
            component: Realtime,
        },
    ],
});

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
